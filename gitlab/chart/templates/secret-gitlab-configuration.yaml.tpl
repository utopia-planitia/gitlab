apiVersion: v1
kind: Secret
metadata:
  name: gitlab-configuration
  annotations:
    secret-generator.v1.mittwald.de/autogenerate: GITLAB_SECRETS_DB_KEY_BASE,GITLAB_SECRETS_OTP_KEY_BASE,GITLAB_SECRETS_SECRET_KEY_BASE
type: Opaque
stringData:
  GITLAB_ROOT_EMAIL: "{{ .Values.root.email }}"
  GITLAB_ROOT_PASSWORD: "{{ .Values.root.password }}"
  GITLAB_HOST: "{{ .Values.domain }}"
  SMTP_USER: "{{ .Values.smtp.username }}"
  SMTP_PASS: "{{ .Values.smtp.password }}"
