#!bash

for team in `ls -1`;
do
	for repo in `ls $team -1`;
	do
		( cd $team/$repo && git gc && git repack -a -f -d --window=250 --depth=250 )
	done
done

